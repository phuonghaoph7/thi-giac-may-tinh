import cv2
import numpy as np
import math
import matplotlib.pyplot as plt
import dlib
#làm sáng
def adjust_gamma(image, gamma):
   invGamma = 1.0 / gamma
   table = np.array([((i / 255.0) ** invGamma) * 255
      for i in np.arange(0, 256)]).astype("uint8")
   return cv2.LUT(image, table)
# lọc mùn, làm mịn
def medianFilter(img):
    median=cv2.medianBlur(img, 5)
    return median
#làm sắc nét
def sacnet(img):
  #làm sắc nét ảnh
  #kernel = np.array(([0, -1, 0],[-1, 5, -1],[0, -1, 0]), dtype="int")
  kernel = np.array(([-1,-1,-1],[-1,9,-1],[-1,-1,-1]), dtype="int")
  img = cv2.filter2D(img, -1, kernel)
  return img

input=cv2.imread('anhgoc.jpg')

output1 = adjust_gamma(input, 2)
output2=medianFilter(input)
output3=sacnet(input)

input = cv2.resize(input, (500, 600))
output1 = cv2.resize(output1, (500, 600))
output2 = cv2.resize(output2, (500, 600))
output3=cv2.resize(output3, (500, 600))
cv2.imshow("anhgoc",input)
cv2.imshow("lamsang",output1)
cv2.imshow("median",output2)
cv2.imshow("lam sac net", output3)

cv2.waitKey(0)
cv2.destroyAllWindows()