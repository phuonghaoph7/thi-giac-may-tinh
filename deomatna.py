import cv2
import numpy as np
import dlib
from math import hypot
# Loading Camera and Nose image and Creating mask
cap = cv2.VideoCapture(0)
nose_image = cv2.imread("matna.png.jpg")

_, frame = cap.read()
rows, cols, _ = frame.shape
nose_mask = np.zeros((rows, cols), np.uint8)
# Loading Face detector
detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")
while True:
    _, frame = cap.read()
    frame = cv2.flip(frame, 1)
    #nose_mask.fill(0)
    gray_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    faces = detector(frame)
    for face in faces:
        landmarks = predictor(gray_frame, face)
        # xác định landmark số 27,0,16
        #top_nose = (landmarks.part(19).x, landmarks.part(19).y)
        center_nose = (landmarks.part(27).x, landmarks.part(27).y)
        left_nose = (landmarks.part(0).x, landmarks.part(0).y)
        right_nose = (landmarks.part(16).x, landmarks.part(16).y)
        #tính kích thươc vùng mặt để ghép mặt nạ
        # hybot() là hàm tính theo công thức sqrt(x*x+y*y)
        nose_width = int(hypot(left_nose[0] - right_nose[0],
                               left_nose[1] - right_nose[1]) * 1.2)
        nose_height = int(nose_width * 1.3)



        top_left = (int(center_nose[0] - nose_width / 2),
                    int(center_nose[1] - nose_height / 2))
        bottom_right = (int(center_nose[0] + nose_width / 2),
                        int(center_nose[1] + nose_height / 2))


        a = np.array([landmarks.part(27).x, landmarks.part(27).y])
        b = np.array([landmarks.part(8).x, landmarks.part(8).y])
        c = np.array([landmarks.part(27).x, 0])
        print(a, b, c)
        ba = a - b
        bc = c - b

        cosine_angle = np.dot(ba, bc) / (np.linalg.norm(ba) * np.linalg.norm(bc))
        if (landmarks.part(8).x >= landmarks.part(27).x):
            q = np.arccos(cosine_angle) * 100
        else:
            q = 180 - np.arccos(cosine_angle) * 100

        num_rows, num_cols = nose_image.shape[:2]


        #print.imshow('anh mat na', nose_image)


       # resize và chuyển mặt nạ về ảnh xám
        nose_pig = cv2.resize(nose_image, (nose_width, nose_height))
        nose_pig_gray = cv2.cvtColor(nose_pig, cv2.COLOR_BGR2GRAY)
        # chuyển về ảnh nhị phân và đảo bit để lấy phần còn lại của mặt nạ( bỏ phần nền)
        _, nose_mask = cv2.threshold(nose_pig_gray, 240, 255, cv2.THRESH_BINARY_INV)
        mask_inv = cv2.bitwise_not(nose_mask)

        rotation_matrix = cv2.getRotationMatrix2D((num_cols / 2, num_rows / 2), q, 0.9)
        nose_image = cv2.warpAffine(nose_image, rotation_matrix, (num_cols, num_rows))

        nose_area = frame[top_left[1]: top_left[1] + nose_height,
                    top_left[0]: top_left[0] + nose_width]
        # vì đang là ảnh nhị phân=> and với ảnh mặt nạ ban đầu( đã resize) để lấy ảnh màu gốc của nó

        masked_face = cv2.bitwise_and(nose_pig,nose_pig, mask=nose_mask)
        # bôi đen( khoanh vùng) cái vị trí để đặt mặt nạ trong khuôn mặt
        nose_area_no_nose=cv2.bitwise_and(nose_area, nose_area,mask=mask_inv)
        # đặt mặt nạ vô đó
        final_nose=cv2.add(masked_face, nose_area_no_nose)
        frame[top_left[1]: top_left[1] + nose_height,
        top_left[0]: top_left[0] + nose_width] = final_nose

        cv2.imshow("final nose", final_nose)
    cv2.imshow("Frame", frame)
    key = cv2.waitKey(1)
    if key == 40:
        break