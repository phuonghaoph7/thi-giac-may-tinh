import cv2
import numpy as np
import matplotlib.pyplot as plt
import dlib
cap = cv2.VideoCapture(0)
face_mask = cv2.imread('matna.png.jpg')
h_mask, w_mask = face_mask.shape[:2]
while True:
    _, frame = cap.read()
    hog_face_detector = dlib.get_frontal_face_detector()
    faces = hog_face_detector(frame,1)
    for face in faces:
        x = face.left()
        y = face.top()
        w = face.right() - x
        h = face.bottom() - y

        if h <= 0 or w <= 0: pass
        h, w = int(1.4*h), int(1.1*w)
        y -= int(0.3*h)
        x -= int(0.1*x)

        # bounding box
        frame_roi = frame[y:y+h, x:x+w]
        # resize mặt nạ về kích thước = kích thước khuộn mặt vừa detect được
        face_mask_small = cv2.resize(face_mask, (w, h),interpolation=cv2.INTER_AREA)

        #chuyển ảnh mặt nạ sang ảnh xám và ảnh nhị phân
        gray_mask = cv2.cvtColor(face_mask_small, cv2.COLOR_BGR2GRAY)
        #cv2_imshow(gray_mask)
        #pixel>245 => đen
        #pixel<245 =>255
        ret, mask = cv2.threshold(gray_mask, 245, 255,cv2.THRESH_BINARY_INV)
        #cv2_imshow(mask)
        # Tạo ảnh nhị phân nghịch đảo
        #sử dụng mặt nạ nghịch đảo để lấy phần còn lại của hình ảnh
        mask_inv = cv2.bitwise_not(mask)
        #cv2_imshow(mask_inv)

        #chỉ lấy vùng mặt nạ từ hình ảnh mặt nạ( không có nên trắng bên ngoài)
        masked_face = cv2.bitwise_and(face_mask_small, face_mask_small,mask=mask)
        #cv2_imshow(face_mask_small)

        # ( bôi đen phần mặt nạ trong roi)
        masked_frame = cv2.bitwise_and(frame_roi, frame_roi,mask=mask_inv)
        #cv2_imshow(masked_frame)
        #masked_frame = cv2.bitwise_and(masked_frame, masked_frame,mask=mask_inv)
        #cv2.imshow(masked_frame)

    #Đặt mặt nạ vào ROI và sửa đổi hình ảnh chính
        frame[y:y + h, x:x + w] = cv2.add(masked_face, masked_frame)
    cv2.imshow("Frame", frame)
    key = cv2.waitKey(1)
    if key == 27:
        break





cv2.destroyAllWindows()