import cv2
import matplotlib.pyplot as plt
import numpy as np


image = cv2.imread('snow.jpg')
image_copy = np.copy(image)
image_copy = cv2.cvtColor(image_copy, cv2.COLOR_BGR2RGB)



lower_black = np.array([0, 0, 0])     ##[R value, G value, B value]
upper_black = np.array([192, 192, 192])
mask = cv2.inRange(image_copy, lower_black, upper_black)
#plt.imshow(mask, cmap='gray')


masked_image = np.copy(image_copy)
masked_image[mask != 0] = [0, 0, 0]
#plt.imshow(masked_image)


background_image = cv2.imread('background.jpg')

background_image = cv2.cvtColor(background_image, cv2.COLOR_BGR2RGB)
crop_background = cv2.resize(background_image, (1000, 667))
crop_background[mask == 0] = [0, 0, 0]
#plt.imshow(crop_background)
plt.show()
final_image = crop_background + masked_image
final_image = cv2.cvtColor(final_image,cv2.COLOR_BGR2RGB)
cv2.imshow( "tuyết",final_image)
cv2.waitKey(0)
cv2.destroyAllWindows()