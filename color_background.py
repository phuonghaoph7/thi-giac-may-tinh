import cv2
import numpy as np

# read the target file
image = 'chipu.jpg'
target_img = cv2.imread(image)
h,r,_ =target_img.shape
# create an image with a single color (here: red)
red_img  = np.full((h,r,3), (0,0,255), np.uint8)
yel_img  = np.full((h,r,3), (0,255,255), np.uint8)
b_img  = np.full((h,r,3), (127,255,127), np.uint8)
#target_img = cv2.resize(target_img, (red_img.shape[1], red_img.shape[0]))
# add the filter  with a weight factor of 20% to ther taget image
fused_img1  = cv2.addWeighted(target_img, 0.8, red_img, 0.2, 0)
fused_img2  = cv2.addWeighted(target_img, 0.8, yel_img, 0.2, 0)
fused_img3  = cv2.addWeighted(target_img, 0.8, b_img, 0.2, 0)
cv2.imshow('anh 1',fused_img1)
cv2.imshow('anh 2',fused_img2)
cv2.imshow('anh 3',fused_img3)
cv2.waitKey(0)